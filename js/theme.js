(function ($, Drupal) {
    Drupal.behaviors.aboutus = {
      attach: function (context, settings) {




        // credit: https://css-tricks.com/working-with-javascript-media-queries/
        // but jquery check for a css class event better I think
        // https://www.fourfront.us/blog/jquery-window-width-and-media-queries/

        $(document).ready(function() {
            // run test on initial page load
            checkSize();
        
            // run test on resize of the window
            $(window).resize(checkSize);




            //$("#thisisme-mob-menu-toggle").

              // Use context instead of document IF DRUPAL.

        });

        //     .thisisme-togl-expand.thisisme-togl-expand--open .menu-text {
        
        // credit:  https://www.fourfront.us/blog/jquery-window-width-and-media-queries/

        //Function to the css rule
        function checkSize(){
            if ($("#block-mainnavigation .menu li").css("display") == "block" ){
                // we're at mobile width, so show the burger menu icon

                // your code here

              // credit: https://stackoverflow.com/a/5360831/227926
               var menuicon = 
                 '<a href="#" id="thisisme-togl-expand" class="thisisme-togl-expand"> \
                   <div class="menu-icon"> \
                   <span class="top"></span> \
                   <span class="middle"></span> \
                   <span class="bottom"></span> \
                   </div> \
                   <div class="menu-text"> \
                   <span class="icon-burger">MENU</span> \
                   <span class="icon-close">CLOSE</span> \
                   </div> \
                 </a>';

                // credit: https://stackoverflow.com/a/6813294/227926
               if ($('#thisisme-mob-menu-toggle').is(':empty')){
                 //do something)
                 $("#thisisme-mob-menu-toggle").append(menuicon);
               }
                


                if ( !$("#thisisme-togl-expand").hasClass("thisisme-togl-expand--open") ) {
                  $(".region-primary-menu").css("display","none");
                }
              
  
               // nearly there - i think this should be a media query break point rather than in js
               //$("#block-clau2-main-menu").toggleClass('thisisme-main-menu-hide');
               /*
               var togl_expd = document.getElementById('thisisme-togl-expand');

               // mobile menu toggle hide/reveal
               togl_expd.addEventListener('click', function (e) {
                 // only for mobile
                  if ($(".thisisme-main-menu li").css("display") == "block" ){
                   togl_expd.classList.toggle('thisisme-togl-expand--open');
                  }
               });
               */

               // need to re-add the event each time because the html is destroyed I think
               // but just in case it's there, want to remove anything that was there before
               // credit: https://stackoverflow.com/a/11613013/227926
$('#thisisme-togl-expand')
  .off('.toglexpandns') // remove all events in namespace upload
  .on({
      'click.toglexpandns': function(e) {
          e.stopPropagation();
          e.preventDefault();
          if ($("#block-mainnavigation .menu li").css("display") == "block" ){
            // a click on the menu burger symbol occured 
            //
            var togl_expd = document.getElementById('thisisme-togl-expand');
            togl_expd.classList.toggle('thisisme-togl-expand--open');

                // nearly there - i think this should be a media query break point rather than in js
            //$("#block-clau2-main-menu").toggleClass('thisisme-main-menu-hide');

            // TODO could put this inside the if and else - opposite settings
            //$(".layout-container").toggleClass('thisisme-overlay-menu-bg');

            if ( $(".region-primary-menu").css("display") == "none" ) {
              $("body").css("overflow","hidden");
              $(".layout-container").addClass('thisisme-overlay-menu-bg');
              $(".layout-container").removeClass('thisisme-unoverlay-menu-bg');

              // menu is not displayed
              //
              // so display it
              //
              // mobile menu overlay is displayed
              $(".region-primary-menu").css("display","inline-block");
              $("#thisisme-hdr").removeClass('thisisme-hdr-hgt-init');
              $("#thisisme-hdr").addClass('thisisme-overlay-hdr-hgt-togl-expand-mob-menu');


              if ( $('#thisisme-hdr').hasClass('thisisme-hdr-home')) {

                $(".thisisme-main-menu").addClass('thisisme-mob-menu-open-home');
                /*
                $(".thisisme-main-menu").css('position', 'absolute');
                $(".thisisme-main-menu").css('top', '7em');
                $(".thisisme-main-menu").css('left', '1.2em');
                */

                /*
                $(".thisisme-main-menu").css('position', 'relative');
                $(".thisisme-main-menu").css('top', '-28em');
                $(".thisisme-main-menu").css('left', '1.2em');
                */
              
              }
              else {
              // not home page
              //
              // reposition displayed menu as per design
              
              $(".thisisme-main-menu").addClass('thisisme-mob-menu-open-non-home');
              /*
              $(".thisisme-main-menu").css('position', 'relative');
              $(".thisisme-main-menu").css('top', '8em');
              $(".thisisme-main-menu").css('left', '-9em');
              */
              }

            }
            else {
              $("body").css("overflow","");
              $(".layout-container").removeClass('thisisme-overlay-menu-bg');
              $(".layout-container").addClass('thisisme-unoverlay-menu-bg');

              $(".region-primary-menu").css("display","none");
              $("#thisisme-hdr").removeClass('thisisme-overlay-hdr-hgt-togl-expand-mob-menu');
              $('#thisisme-hdr').addClass('thisisme-hdr-hgt-init');

              // reset position of menu when not in use

              $(".thisisme-main-menu").removeClass('thisisme-mob-menu-open-non-home');
              $(".thisisme-main-menu").removeClass('thisisme-mob-menu-open-home');
              //
              $(".thisisme-main-menu").css('position', '');
              $(".thisisme-main-menu").css('top', '');
              $(".thisisme-main-menu").css('left', '');
            }

            // these two classes are never on at the same time
            //
            // initially on, so a toggle will turn it off

            //
            // initially off, so a toggle will turn it on




          }
          else {
            // click on menu burger occured but we're not at mobile width

            $(".region-primary-menu").css("display","inline-block");
            $("#thisisme-hdr").addClass('thisisme-hdr-hgt-init');

            // reset position of menu when not in use
            $(".thisisme-main-menu").removeClass('thisisme-mob-menu-open-non-home');
            $(".thisisme-main-menu").removeClass('thisisme-mob-menu-open-home');
            //
            $(".thisisme-main-menu").css('position', '');
            $(".thisisme-main-menu").css('top', '');
            $(".thisisme-main-menu").css('left', '');

            //$("#thisisme-hdr").removeClass('thisisme-overlay-hdr-hgt-togl-expand-mob-menu');

           // $(".region-primary-menu").css("display","none");
            // nearly there - i think this should be a media query break point rather than in js
            //$("#block-clau2-main-menu").toggleClass('thisisme-main-menu-hide');
          }
      }
      // ...
  });

  // other refs:
  // https://stackoverflow.com/questions/5963669/whats-the-difference-between-event-stoppropagation-and-event-preventdefault
  // https://api.jquery.com/on/#event-names
               /* TODO

  function isEmpty( el ){
      return !$.trim(el.html())
  }
  if (isEmpty($('#element'))) {
      // do something
  }

               */
            }
            else {
               // we're not at mobile width

                $("#thisisme-mob-menu-toggle").empty();

                // nearly there - i think this should be a media query break point rather than in js
                //$("#block-clau2-main-menu").toggleClass('thisisme-main-menu-hide');

                $("body").css("overflow","");
                $(".layout-container").removeClass('thisisme-overlay-menu-bg');
                $(".layout-container").addClass('thisisme-unoverlay-menu-bg');

                $(".region-primary-menu").css("display","inline-block");

                $("#thisisme-hdr").addClass('thisisme-hdr-hgt-init');

                // remove mobile positioning of menu so that menu displayed in correct position at non-mobile widths
                $(".thisisme-main-menu").removeClass('thisisme-mob-menu-open-non-home');
                $(".thisisme-main-menu").removeClass('thisisme-mob-menu-open-home');
                //
                $(".thisisme-main-menu").css('position', '');
                $(".thisisme-main-menu").css('top', '');
                $(".thisisme-main-menu").css('left', '');

                if ( $('#thisisme-hdr').hasClass('thisisme-hdr-home')) {
                  $('#thisisme-hdr').empty();
                }
            }


        } // end checksize

/*

@media screen and (max-width: 767px ) {
  .thisisme-main-menu li {
    margin-right: 1.15em;
    display: block;
    padding-bottom: 1.5em;
  }

*/


/*
        function handleTabletChange(e) {
        // Check if the media query is true
          if (e.matches) {
            // Then log the following message to the console
            console.log('Media Query Matched!');
          }
        }


*/

        
        


        //
      }
    };


  })(jQuery, Drupal);
  